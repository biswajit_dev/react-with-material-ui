import React from "react";

import "./App.css";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import Delete from "@material-ui/icons/Delete";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import ToolBar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";


import { makeStyles, ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { deepOrange, yellow } from "@material-ui/core/colors";


const useStyles = makeStyles({
  root: {
    background: 'linear-gradient(45deg, #FE6888, #FF8E53)',
    border: 0,
    margin: '20px',
    color: '#fff',
    padding: '15px 15px',
    width: '200px'
  }
})

const theme = createMuiTheme({
  palette: {
    primary: {
      main: deepOrange[200]
    },
    secondary: {
      main: yellow[100]
    }
  }
})

const ButtonStyled = () => {
  const classes = useStyles();
  return <Button className={classes.root}>Custom Button</Button>
}

const App = () => {
  const [check, setCheck] = React.useState(false);
  const [login, setLogin] = React.useState(false);

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="xl">
        <AppBar>
          <ToolBar>
            <IconButton>
              <MenuIcon/>
            </IconButton>
            <Typography variant="h6">
              Material Study
            </Typography>
            <Button onClick={()=> setLogin(!login)}>
              {
                login ? "Logout" : "Login"
              }
            </Button>
          </ToolBar>
        </AppBar>
        <div className="main-container">
          <Typography variant="h3">
            Hello Developer
        </Typography>
          <FormControlLabel
            control={
              <Checkbox
                checked={check}
                onChange={(e) => setCheck(!check)}
                color='secondary'
                inputProps={{
                  'arial-label': 'secondary checkbox'
                }}
              />}
            label="Show button"
          />
          <ButtonStyled />
          <TextField
            type='date'
            color='primary'
            variant='standard'
            label='Date'
            style={{width: 250}}
          />
          <Grid container spacing="2" justify="center">
            <Grid item>
                <Paper style={{height: 180, width: 120, color: "aliceblue"}}/>
            </Grid>
            <Grid item>
                <Paper style={{height: 180, width: 120, color: "aliceblue"}}/>
            </Grid>            
            <Grid item>
                <Paper style={{height: 180, width: 120, color: "aliceblue"}}/>
            </Grid>            
            <Grid item>
                <Paper style={{height: 180, width: 120, color: "aliceblue"}}/>
            </Grid>            
            <Grid item>
                <Paper style={{height: 180, width: 120, color: "aliceblue"}}/>
            </Grid>
            <Grid item>
                <Paper style={{height: 180, width: 120, color: "aliceblue"}}/>
            </Grid>
          </Grid>

          {
            check && (<ButtonGroup>
              <Button
                startIcon={<SaveIcon />}
                variant='contained'
                color='primary'>
                Save
            </Button>
              <Button
                startIcon={<Delete />}
                variant='contained'
                color='secondary'>
                Cancel
            </Button>
            </ButtonGroup>)
          }
        </div>
      </Container>
    </ThemeProvider>
  )
}

export default App;